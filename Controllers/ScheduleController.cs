﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymWebApp.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace GymWebApp.Controllers
{
    public class ScheduleController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Schedule
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Activities.ToList());
        }

        // GET: Schedule/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Activity activity = db.Activities.Find(id);
        //    if (activity == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(activity);
        //}

        // GET: Schedule/Create
        [Authorize(Roles = "ADMIN")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Schedule/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ADMIN")]
        public ActionResult Create([Bind(Include = "ID,Name,Day,Hour")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Activities.Add(activity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(activity);
        }

        // GET: Schedule/Edit/5
        [Authorize(Roles = "ADMIN")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: Schedule/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ADMIN")]
        public ActionResult Edit([Bind(Include = "ID,Name,Day,Hour")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(activity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(activity);
        }

        // GET: Schedule/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            db.Activities.Remove(activity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Schedule/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Activity activity = db.Activities.Find(id);
        //    db.Activities.Remove(activity);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        // GET: Schedule/SignIn/5
        [Authorize(Roles = "CLIENT")]
        public ActionResult SignIn(int? id) 
        {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null) {
                return HttpNotFound();
            }
            if(activity.IsFull) {
                return View("Index");
            }
            else {
                ApplicationUser appUser = db.Users.Find(User.Identity.GetUserId());
                if(!appUser.Activities.Contains(activity)) {

                    if(activity.SignInUser(appUser)) {
                        appUser.Activities.Add(activity);

                        db.Entry(appUser).State = EntityState.Modified;
                        db.Entry(activity).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
        }

        // GET: Schedule/SignOut/5
        [Authorize(Roles = "CLIENT")]
        public ActionResult SignOut(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null) {
                return HttpNotFound();
            }
            else {
                ApplicationUser appUser = db.Users.Find(User.Identity.GetUserId());
                if(appUser.Activities.Contains(activity)) {
                    appUser.Activities.Remove(activity);
                    activity.SignOutUser(appUser);

                    db.Entry(appUser).State = EntityState.Modified;
                    db.Entry(activity).State = EntityState.Modified;

                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
        }


        // GET: Schedule/My
        [Authorize(Roles = "CLIENT")]
        public ActionResult My() {
            return View(db.Users.Find(User.Identity.GetUserId()).Activities.OrderBy(a => a.Hour).OrderBy(a => a.Day));
        }

    }
}
