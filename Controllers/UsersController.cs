﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymWebApp.Models;
using System.Data.Entity.Infrastructure;

namespace GymWebApp.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize(Roles = "ADMIN")]
        public ActionResult Index()
        {
            ViewData["Phrase"] = "";
            string roleId = db.Roles.First(role => role.Name == "CLIENT").Id;
            return View(db.Users.Where(user => user.Roles.Any(r => r.RoleId == roleId)));
        }

        [Authorize(Roles = "ADMIN")]
        public ActionResult Search(string phrase) {
            ViewData["Phrase"] = phrase;
            string roleId = db.Roles.First(role => role.Name == "CLIENT").Id;
            return View("Index", db.Users.Where(user => user.Roles.Any(r => r.RoleId == roleId) && user.Email.Contains(phrase)));
        }

        [Authorize(Roles = "ADMIN")]
        public ActionResult Details(string id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db.Users.Find(id);
            if (user == null) {
                return HttpNotFound();
            }
            return View(user);
        }

        [Authorize(Roles = "ADMIN")]
        public ActionResult Edit(string id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db.Users.Find(id);
            if (user == null) {
                return HttpNotFound();
            }
            return View(new EditUserViewModel { ID=user.Id, Email=user.Email, ProfileId=user.MyProfile.ID, FirstName = user.MyProfile.FirstName, LastName = user.MyProfile.LastName });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ADMIN")]
        public ActionResult Edit(EditUserViewModel m, HttpPostedFileBase upload) {
            ApplicationUser userToUpdate = db.Users.Find(m.ID);
            userToUpdate.Email = m.Email;
            userToUpdate.UserName = m.Email;

            Profile profileToUpdate = userToUpdate.MyProfile;
            profileToUpdate.FirstName = m.FirstName;
            profileToUpdate.LastName = m.LastName;
            try {
                if (ModelState.IsValid) {
                    if (upload != null && upload.ContentLength > 0) {
                        using (var reader = new System.IO.BinaryReader(upload.InputStream)) {
                            profileToUpdate.Avatar = reader.ReadBytes(upload.ContentLength);
                            profileToUpdate.Type = upload.ContentType;
                        }
                    }
                    db.Entry(userToUpdate).State = EntityState.Modified;
                    db.Entry(profileToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Details", new { id = m.ID });
                }
            }
            catch (RetryLimitExceededException) {
                ModelState.AddModelError("", "Unable to save changes.");
            }
            return View("Edit", new { id = m.ID });
        }

        //// GET: Users/Delete/5
        //public ActionResult Delete(string id) {
        //    if (id == null) {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ApplicationUser applicationUser = db.ApplicationUsers.Find(id);
        //    if (applicationUser == null) {
        //        return HttpNotFound();
        //    }
        //    return View(applicationUser);
        //}

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db.Users.Find(id);
            if (user == null) {
                return HttpNotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
