﻿using GymWebApp.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GymWebApp.Controllers
{
    public class ProfileController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Profile/Edit
        [Authorize(Roles = "CLIENT")]
        public ActionResult Edit()
        {
            return View(db.Users.Find(User.Identity.GetUserId()).MyProfile);
        }

        // POST: Profile/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "CLIENT")]
        public ActionResult Edit(string FirstName, string LastName, HttpPostedFileBase upload) {
            Profile profileToUpdate = db.Users.Find(User.Identity.GetUserId()).MyProfile;
            profileToUpdate.FirstName = FirstName;
            profileToUpdate.LastName = LastName;
            try {
                if (ModelState.IsValid) {
                    if (upload != null && upload.ContentLength > 0) {
                        using (var reader = new System.IO.BinaryReader(upload.InputStream)) {
                            profileToUpdate.Avatar = reader.ReadBytes(upload.ContentLength);
                            profileToUpdate.Type = upload.ContentType;
                        }
                    }
                    db.Entry(profileToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Edit");
                }
            }
            catch (RetryLimitExceededException) {
                ModelState.AddModelError("", "Unable to save changes.");
            }
            return View("Edit");
        }
    }
}