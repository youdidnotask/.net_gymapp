﻿using GymWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GymWebApp.Controllers
{
    public class FileController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: File
        public ActionResult Index(int id)
        {
            return File(db.Profiles.Find(id).Avatar, db.Profiles.Find(id).Type);
        }
    }
}