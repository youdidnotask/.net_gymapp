﻿using GymWebApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GymWebApp.Startup))]
namespace GymWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRoles();
        }

        private void CreateRoles() {
            ApplicationDbContext db = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (!roleManager.RoleExists("ADMIN")) {
                var role = new IdentityRole();
                role.Name = "ADMIN";
                roleManager.Create(role);

                ApplicationUser user = new ApplicationUser();
                user.UserName = "admin@gmail.com";
                user.Email = "admin@gmail.com";
                string password = "aA1!aa";

                var result = userManager.Create(user, password);
                if (result.Succeeded) {
                    userManager.AddToRole(user.Id, "ADMIN");
                }
            }
            if (!roleManager.RoleExists("EMPLOYEE")) {
                var role = new IdentityRole();
                role.Name = "EMPLOYEE";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("CLIENT")) {
                var role = new IdentityRole();
                role.Name = "CLIENT";
                roleManager.Create(role);
            }
        }
    }
}
