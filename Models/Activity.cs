﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GymWebApp.Models {
    public enum DayOfWeek {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY
    }

    public class Activity {
        public static readonly UInt32 Limit = 4;
        public int ID { get; set; }
        public string Name { get; set; }
        public DayOfWeek Day { get; set; }
        public string Hour { get; set; }
        public bool IsFull { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }

        public Activity() {
            this.Users = new HashSet<ApplicationUser>();
            IsFull = false;
        }

        public bool IsSigned(string id) {
            foreach(ApplicationUser user in Users) {
                if(user.Id == id) {
                    return true;
                }
            }
            return false;
        }

        public bool SignInUser(ApplicationUser user) {
            if(!IsFull && !Users.Contains(user)) {
                Users.Add(user);
                if (Users.Count == Limit) {
                    IsFull = true;
                }
                return true;
            }
            return false;
        }

        public void SignOutUser(ApplicationUser user) {
            Users.Remove(user);
            if (Users.Count < Limit) {
                IsFull = false;
            }
        }
    }
}