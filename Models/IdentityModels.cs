﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace GymWebApp.Models 
{
    public class Profile {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Avatar { get; set; }
        public string Type { get; set; }

        public Profile() {
            FirstName = "None";
            LastName = "None";
            LoadDefaultAvatar();
        }

        private void LoadDefaultAvatar() {
            Image image = Image.FromFile(@"D:\informatyka6\PABWT .NET\GymSolution\GymWebApp\Images\user_default.png");
            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            Avatar =  ms.ToArray();
            Type = "image / png";
        }
    }

    public class ApplicationUser : IdentityUser
    {
        [ForeignKey("ProfileID")]
        public virtual Profile MyProfile { get; set; }
        public int? ProfileID { get; set; }
        public virtual ICollection<Activity> Activities { get; set; }

        public ApplicationUser() : base() {
            this.Activities = new HashSet<Activity>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public bool HasActivity(Activity activity) {
            return Activities.Contains(activity);
        }
        //public bool HasRole(string role) {
        //    //foreach(IdentityUserRole r in Roles) {
        //    //    if(r.Equals(new IdentityUserRole()) ) {
        //    //        return true;
        //    //    }
        //    //}
        //    //return false;
        //}
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<GymWebApp.Models.Activity> Activities { get; set; }

        public System.Data.Entity.DbSet<GymWebApp.Models.Profile> Profiles { get; set; }

        //public System.Data.Entity.DbSet<GymWebApp.Models.ApplicationUser> ApplicationUsers { get; set; }

        //public System.Data.Entity.DbSet<GymWebApp.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}